# Project disco

The world's _best_ motion-activated blinkenlights for pinewood derby cars.


### Hardware:

![wiring](img/wiring.png)


#### Speaker
Not polarized, attach speaker wires to pads

##### I2S 

IC: MAX98357

Pins: 
* DOUT: GPIO18
* LRCLK: GPIO21
* BCLK: GPIO19

#### Battery

JST PH 2-pin.  Positive pin is on the left, as shown in image.

#### Serial Port

The serial port is 3.3V TTL.

Pin names are from DTE perspective, so the `TX` from your TTL RS-232 device would attach to the `TX` on this board.  Do _not_ do `RX` --> `TX`, `TX` --> `RX`.

Since Project Disco does not use an auto-reset circuit, connecting via normal term program probably won't work as expected.  Use the `fw/monitor` script instead.  

#### LED Port

The `+` and `-` pins are for power, connected directly to the battery.  "D" is the data port, GPIO14 on the ESP32.


<table border=0>
<tr><td> <img src="img/achtung.png"></td>
<td> <B>IMPORTANT</b> : R1.0 version of this PCB have the `+` and `-` pin _labels_ swapped.  The actual polarity on all versions is as shown in the image above</td>
</tr>
</table>

### Schematic

![Schematic](img/schematic.png)
[PDF](img/schematic.pdf)

### Firmware

* Base Image: [Micropython v1.9.4](https://github.com/micropython/micropython/tree/v1.9.4)
* Apply this PR: [esp32-i2s](https://github.com/miketeachman/micropython/tree/esp32-i2s)
  *  Discussion here: [esp32: add support for I2S](https://github.com/micropython/micropython/pull/4471)

Be sure to do ensure your ESP-IDF aligns with the proper git hash for Micropython, and remember to `git submodule update --init --recursive` in both the ESP-IDF and Micropython repositories.

Also a good idea to do a `clean` in Micropython. Before building


#### Examples

Be sure to check the `fw` directory for drivers, if any.

##### MPU6050:
```
import mpu6050

m = mpu6050.MPU6050()
while 1: m.read_sensors_scaled()
```

##### I2S

See `fw/examples/play-mono-wav.py`


##### LEDs

TBD






