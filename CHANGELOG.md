

# R1.0
Initial relase

# R1.1
* Added R103 as a pullup for `en` pin
* Fixed silkscreen for LED polarity
* Added `+` to silkscreen for battery polarity
* Increased trace width for LED ground pad
