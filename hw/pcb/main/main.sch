EESchema Schematic File Version 4
LIBS:main-cache
EELAYER 26 0
EELAYER END
$Descr USLedger 17000 11000
encoding utf-8
Sheet 1 1
Title "Project Disco"
Date "2019-03-28"
Rev "1.1"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L RF_Module:ESP32-WROOM-32 U101
U 1 1 5C4669FE
P 5300 3750
F 0 "U101" H 4500 5250 50  0000 C CNN
F 1 "ESP32-WROOM-32" H 4800 5150 50  0000 C CNN
F 2 "RF_Module:ESP32-WROOM-32" H 5300 2250 50  0001 C CNN
F 3 "https://www.espressif.com/sites/default/files/documentation/esp32-wroom-32_datasheet_en.pdf" H 5000 3800 50  0001 C CNN
	1    5300 3750
	1    0    0    -1  
$EndComp
Wire Wire Line
	1850 2150 2650 2150
Wire Wire Line
	1850 2250 2650 2250
Wire Wire Line
	1850 2350 2650 2350
Wire Wire Line
	1850 2450 2650 2450
Wire Wire Line
	1850 2550 2650 2550
Wire Wire Line
	1850 2650 2650 2650
Wire Wire Line
	5900 2550 6750 2550
Wire Wire Line
	5900 2650 6750 2650
Wire Wire Line
	5900 2750 6750 2750
Wire Wire Line
	5900 2850 6750 2850
Wire Wire Line
	5900 2950 6750 2950
Wire Wire Line
	5900 3050 6750 3050
Wire Wire Line
	5900 3150 6750 3150
Wire Wire Line
	5900 3250 6750 3250
Wire Wire Line
	5900 3350 6750 3350
Wire Wire Line
	5900 3450 6750 3450
Wire Wire Line
	5900 3550 6750 3550
Wire Wire Line
	5900 3650 6750 3650
Wire Wire Line
	5900 3750 6750 3750
Wire Wire Line
	5900 3850 6750 3850
Wire Wire Line
	5900 3950 6750 3950
Wire Wire Line
	5900 4050 6750 4050
Wire Wire Line
	5900 4150 6750 4150
Wire Wire Line
	5900 4250 6750 4250
Wire Wire Line
	5900 4350 6750 4350
Wire Wire Line
	5900 4450 6750 4450
Wire Wire Line
	5900 4550 6750 4550
Wire Wire Line
	5900 4650 6750 4650
Wire Wire Line
	5900 4750 6750 4750
Wire Wire Line
	5900 4850 6750 4850
Wire Wire Line
	3850 2550 4700 2550
Wire Wire Line
	3850 3750 4700 3750
Wire Wire Line
	3850 3850 4700 3850
Wire Wire Line
	3850 3950 4700 3950
Wire Wire Line
	3850 4050 4700 4050
Wire Wire Line
	3850 4150 4700 4150
Wire Wire Line
	3850 4250 4700 4250
Wire Wire Line
	3850 2750 4700 2750
Wire Wire Line
	3850 2850 4700 2850
Text Label 2650 2150 2    50   ~ 0
EN
Text Label 2650 2250 2    50   ~ 0
TX
Text Label 2650 2350 2    50   ~ 0
RX
Text Label 2650 2450 2    50   ~ 0
VDD
Text Label 2650 2550 2    50   ~ 0
0
Text Label 2650 2650 2    50   ~ 0
GND
Text Label 6750 2550 2    50   ~ 0
0
Text Label 6750 2650 2    50   ~ 0
TX
Text Label 6750 2850 2    50   ~ 0
RX
$Comp
L power:GND #PWR0101
U 1 1 5C468B64
P 5300 5250
F 0 "#PWR0101" H 5300 5000 50  0001 C CNN
F 1 "GND" H 5305 5077 50  0000 C CNN
F 2 "" H 5300 5250 50  0001 C CNN
F 3 "" H 5300 5250 50  0001 C CNN
	1    5300 5250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 5150 5300 5250
$Comp
L power:VDD #PWR0102
U 1 1 5C469738
P 5300 2250
F 0 "#PWR0102" H 5300 2100 50  0001 C CNN
F 1 "VDD" H 5317 2423 50  0000 C CNN
F 2 "" H 5300 2250 50  0001 C CNN
F 3 "" H 5300 2250 50  0001 C CNN
	1    5300 2250
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 2250 5300 2350
$Comp
L Connector_Generic:Conn_01x02 J2
U 1 1 5C46B0FC
P 2250 4850
F 0 "J2" H 2329 4842 50  0000 L CNN
F 1 "S2B-PH-SM4-TB(LF)(SN)" H 2329 4751 50  0000 L CNN
F 2 "project_disco:JST_PH_S2B-PH-SM4-TB_1x02-1MP_P2.00mm_Horizontal" H 2250 4850 50  0001 C CNN
F 3 "~" H 2250 4850 50  0001 C CNN
F 4 "S2B-PH-SM4-TB(LF)(SN)" H 2250 4850 50  0001 C CNN "MPN"
	1    2250 4850
	1    0    0    -1  
$EndComp
Wire Wire Line
	2050 4850 1400 4850
Wire Wire Line
	2050 4950 1400 4950
Text Label 1400 4950 0    50   ~ 0
GND
Text Label 1400 4850 0    50   ~ 0
VBATT
$Comp
L Regulator_Linear:AP2112K-3.3 U2
U 1 1 5C46C12D
P 3500 5950
F 0 "U2" H 3500 6292 50  0000 C CNN
F 1 "AP2112K-3.3" H 3500 6201 50  0000 C CNN
F 2 "Package_TO_SOT_SMD:SOT-23-5" H 3500 6275 50  0001 C CNN
F 3 "https://www.diodes.com/assets/Datasheets/AP2112.pdf" H 3500 6050 50  0001 C CNN
F 4 "AP2112K-3.3TRG1" H 3500 5950 50  0001 C CNN "MPN"
	1    3500 5950
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5C46C1DC
P 3500 6300
F 0 "#PWR0103" H 3500 6050 50  0001 C CNN
F 1 "GND" H 3505 6127 50  0000 C CNN
F 2 "" H 3500 6300 50  0001 C CNN
F 3 "" H 3500 6300 50  0001 C CNN
	1    3500 6300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3500 6250 3500 6300
Wire Wire Line
	3800 5850 4200 5850
Wire Wire Line
	2800 5850 3200 5850
Wire Wire Line
	2800 5950 3200 5950
Wire Wire Line
	2800 5850 2800 5950
Text Label 2650 5850 0    50   ~ 0
VBATT
Wire Wire Line
	2650 5850 2800 5850
Connection ~ 2800 5850
Text Label 4200 5850 2    50   ~ 0
VDD
$Comp
L Sensor_Motion:MPU-6050 U102
U 1 1 5C471F05
P 8925 3350
F 0 "U102" H 8425 2600 50  0000 C CNN
F 1 "MPU-6050" H 8575 2700 50  0000 C CNN
F 2 "Sensor_Motion:InvenSense_QFN-24_4x4mm_P0.5mm" H 8925 2550 50  0001 C CNN
F 3 "https://store.invensense.com/datasheets/invensense/MPU-6050_DataSheet_V3%204.pdf" H 8925 3200 50  0001 C CNN
F 4 "CL21B103KAANNNC" H 8925 3350 50  0001 C CNN "MPN"
	1    8925 3350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0104
U 1 1 5C471FCA
P 8925 4150
F 0 "#PWR0104" H 8925 3900 50  0001 C CNN
F 1 "GND" H 8930 3977 50  0000 C CNN
F 2 "" H 8925 4150 50  0001 C CNN
F 3 "" H 8925 4150 50  0001 C CNN
	1    8925 4150
	1    0    0    -1  
$EndComp
Wire Wire Line
	8925 4050 8925 4150
Wire Wire Line
	9025 2650 9025 2500
Wire Wire Line
	9025 2500 8925 2500
Wire Wire Line
	8825 2500 8825 2650
$Comp
L power:VDD #PWR0105
U 1 1 5C474323
P 8925 2050
F 0 "#PWR0105" H 8925 1900 50  0001 C CNN
F 1 "VDD" H 8942 2223 50  0000 C CNN
F 2 "" H 8925 2050 50  0001 C CNN
F 3 "" H 8925 2050 50  0001 C CNN
	1    8925 2050
	1    0    0    -1  
$EndComp
Wire Wire Line
	8925 2050 8925 2200
Connection ~ 8925 2500
Wire Wire Line
	8925 2500 8825 2500
Text Label 4150 2550 0    50   ~ 0
P3
Text Label 4150 2750 0    50   ~ 0
P4
Text Label 4150 2850 0    50   ~ 0
P5
Text Label 6300 4750 2    50   ~ 0
P6
Text Label 6300 4850 2    50   ~ 0
P7
Text Label 6300 4250 2    50   ~ 0
P10
Text Label 6300 4350 2    50   ~ 0
P11
Text Label 6300 4450 2    50   ~ 0
P12
Text Label 6300 3350 2    50   ~ 0
P13
Text Label 6300 3150 2    50   ~ 0
P14
Text Label 6300 3250 2    50   ~ 0
P16
Text Label 4150 3950 0    50   ~ 0
P17
Text Label 4150 4050 0    50   ~ 0
P18
Text Label 4150 4250 0    50   ~ 0
P19
Text Label 4150 4150 0    50   ~ 0
P20
Text Label 4150 3750 0    50   ~ 0
P21
Text Label 4150 3850 0    50   ~ 0
P22
Text Label 6300 3450 2    50   ~ 0
P23
Text Label 6300 2750 2    50   ~ 0
P24
Text Label 6300 2550 2    50   ~ 0
P25
Text Label 6300 2950 2    50   ~ 0
P26
Text Label 6300 3550 2    50   ~ 0
P27
Text Label 6300 3650 2    50   ~ 0
P28
Text Label 6300 3050 2    50   ~ 0
P29
Text Label 6300 3750 2    50   ~ 0
P30
Text Label 6300 3850 2    50   ~ 0
P31
Text Label 6300 3950 2    50   ~ 0
P33
Text Label 6300 4050 2    50   ~ 0
P36
Text Label 6300 4150 2    50   ~ 0
P37
Text Label 3850 2550 0    50   ~ 0
EN
Wire Wire Line
	8225 3050 7850 3050
Wire Wire Line
	8225 3150 7850 3150
Wire Wire Line
	8225 3550 7850 3550
Wire Wire Line
	8225 3650 7850 3650
Wire Wire Line
	10000 3050 9625 3050
Wire Wire Line
	10000 3350 9625 3350
Wire Wire Line
	10000 3250 9625 3250
Text Label 6750 4550 2    50   ~ 0
SDA
Text Label 6750 4650 2    50   ~ 0
SCL
Text Label 7850 3150 0    50   ~ 0
SCL
Text Label 7850 3050 0    50   ~ 0
SDA
Wire Wire Line
	7600 3250 7600 4050
Wire Wire Line
	7600 4050 8925 4050
Wire Wire Line
	7600 3250 8225 3250
Connection ~ 8925 4050
$Comp
L Device:R R101
U 1 1 5C51FBE6
P 7100 1775
F 0 "R101" H 7170 1821 50  0000 L CNN
F 1 "10k" H 7170 1730 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 7030 1775 50  0001 C CNN
F 3 "~" H 7100 1775 50  0001 C CNN
	1    7100 1775
	1    0    0    -1  
$EndComp
$Comp
L Device:R R102
U 1 1 5C51FD48
P 7450 1775
F 0 "R102" H 7520 1821 50  0000 L CNN
F 1 "10k" H 7520 1730 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 7380 1775 50  0001 C CNN
F 3 "~" H 7450 1775 50  0001 C CNN
	1    7450 1775
	1    0    0    -1  
$EndComp
Wire Wire Line
	7450 1925 7450 2175
Wire Wire Line
	7100 1925 7100 2175
$Comp
L power:VDD #PWR0106
U 1 1 5C524AEE
P 7275 1525
F 0 "#PWR0106" H 7275 1375 50  0001 C CNN
F 1 "VDD" H 7292 1698 50  0000 C CNN
F 2 "" H 7275 1525 50  0001 C CNN
F 3 "" H 7275 1525 50  0001 C CNN
	1    7275 1525
	1    0    0    -1  
$EndComp
Wire Wire Line
	7100 1625 7100 1525
Wire Wire Line
	7100 1525 7275 1525
Wire Wire Line
	7275 1525 7450 1525
Wire Wire Line
	7450 1525 7450 1625
Connection ~ 7275 1525
Text Label 7450 2175 0    50   ~ 0
SDA
Text Label 7100 2175 0    50   ~ 0
SCL
$Comp
L Connector_Generic:Conn_01x01 J101
U 1 1 5C52F407
P 1650 2150
F 0 "J101" H 1800 2150 50  0000 C CNN
F 1 "Conn_01x01" H 1570 2016 50  0001 C CNN
F 2 "TestPoint:TestPoint_Pad_1.5x1.5mm" H 1650 2150 50  0001 C CNN
F 3 "~" H 1650 2150 50  0001 C CNN
	1    1650 2150
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J102
U 1 1 5C52F54C
P 1650 2250
F 0 "J102" H 1800 2250 50  0000 C CNN
F 1 "Conn_01x01" H 1570 2116 50  0001 C CNN
F 2 "TestPoint:TestPoint_Pad_1.5x1.5mm" H 1650 2250 50  0001 C CNN
F 3 "~" H 1650 2250 50  0001 C CNN
	1    1650 2250
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J103
U 1 1 5C52F570
P 1650 2350
F 0 "J103" H 1800 2350 50  0000 C CNN
F 1 "Conn_01x01" H 1570 2216 50  0001 C CNN
F 2 "TestPoint:TestPoint_Pad_1.5x1.5mm" H 1650 2350 50  0001 C CNN
F 3 "~" H 1650 2350 50  0001 C CNN
	1    1650 2350
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J104
U 1 1 5C52F607
P 1650 2450
F 0 "J104" H 1800 2450 50  0000 C CNN
F 1 "Conn_01x01" H 1570 2316 50  0001 C CNN
F 2 "TestPoint:TestPoint_Pad_1.5x1.5mm" H 1650 2450 50  0001 C CNN
F 3 "~" H 1650 2450 50  0001 C CNN
	1    1650 2450
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J105
U 1 1 5C52F60D
P 1650 2550
F 0 "J105" H 1800 2550 50  0000 C CNN
F 1 "Conn_01x01" H 1570 2416 50  0001 C CNN
F 2 "TestPoint:TestPoint_Pad_1.5x1.5mm" H 1650 2550 50  0001 C CNN
F 3 "~" H 1650 2550 50  0001 C CNN
	1    1650 2550
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J106
U 1 1 5C52F613
P 1650 2650
F 0 "J106" H 1800 2650 50  0000 C CNN
F 1 "Conn_01x01" H 1570 2516 50  0001 C CNN
F 2 "TestPoint:TestPoint_Pad_1.5x1.5mm" H 1650 2650 50  0001 C CNN
F 3 "~" H 1650 2650 50  0001 C CNN
	1    1650 2650
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J107
U 1 1 5C531664
P 1650 3375
F 0 "J107" H 1800 3375 50  0000 C CNN
F 1 "Conn_01x01" H 1570 3241 50  0001 C CNN
F 2 "TestPoint:TestPoint_Pad_2.5x2.5mm" H 1650 3375 50  0001 C CNN
F 3 "~" H 1650 3375 50  0001 C CNN
	1    1650 3375
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J108
U 1 1 5C5317E7
P 1650 3225
F 0 "J108" H 1800 3225 50  0000 C CNN
F 1 "Conn_01x01" H 1570 3091 50  0001 C CNN
F 2 "TestPoint:TestPoint_Pad_2.5x2.5mm" H 1650 3225 50  0001 C CNN
F 3 "~" H 1650 3225 50  0001 C CNN
	1    1650 3225
	-1   0    0    1   
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J109
U 1 1 5C531825
P 1650 3075
F 0 "J109" H 1800 3075 50  0000 C CNN
F 1 "Conn_01x01" H 1570 2941 50  0001 C CNN
F 2 "TestPoint:TestPoint_Pad_2.5x2.5mm" H 1650 3075 50  0001 C CNN
F 3 "~" H 1650 3075 50  0001 C CNN
	1    1650 3075
	-1   0    0    1   
$EndComp
Wire Wire Line
	1850 3075 2650 3075
Wire Wire Line
	1850 3375 2650 3375
Wire Wire Line
	1850 3225 2650 3225
Text Label 2650 3075 2    50   ~ 0
VBATT
Text Label 2650 3375 2    50   ~ 0
GND
Text Label 2650 3225 2    50   ~ 0
LED_DATA
Text Label 6750 3350 2    50   ~ 0
LED_DATA
$Comp
L project_disco:MAX98357-QFN U103
U 1 1 5C5213DA
P 8700 5325
F 0 "U103" H 7900 4875 50  0000 L CNN
F 1 "MAX98357-QFN" H 7900 4950 50  0000 L CNN
F 2 "Package_DFN_QFN:QFN-16-1EP_3x3mm_P0.5mm_EP2.7x2.7mm_ThermalVias" H 8700 5325 50  0001 C CNN
F 3 "" H 8700 5325 50  0001 C CNN
	1    8700 5325
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J110
U 1 1 5C52151C
P 9900 5225
F 0 "J110" H 10050 5225 50  0000 C CNN
F 1 "Conn_01x01" H 9820 5091 50  0001 C CNN
F 2 "TestPoint:TestPoint_Pad_2.5x2.5mm" H 9900 5225 50  0001 C CNN
F 3 "~" H 9900 5225 50  0001 C CNN
	1    9900 5225
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x01 J111
U 1 1 5C521820
P 9900 5425
F 0 "J111" H 10050 5425 50  0000 C CNN
F 1 "Conn_01x01" H 9820 5291 50  0001 C CNN
F 2 "TestPoint:TestPoint_Pad_2.5x2.5mm" H 9900 5425 50  0001 C CNN
F 3 "~" H 9900 5425 50  0001 C CNN
	1    9900 5425
	1    0    0    -1  
$EndComp
Wire Wire Line
	9500 5225 9700 5225
Wire Wire Line
	9700 5425 9500 5425
Wire Wire Line
	7900 5125 7325 5125
Wire Wire Line
	7900 5225 7325 5225
Wire Wire Line
	7900 5325 7325 5325
Wire Wire Line
	7900 5425 7325 5425
Wire Wire Line
	7900 5525 7325 5525
$Comp
L power:GND #PWR0107
U 1 1 5C528335
P 8750 5925
F 0 "#PWR0107" H 8750 5675 50  0001 C CNN
F 1 "GND" H 8755 5752 50  0000 C CNN
F 2 "" H 8750 5925 50  0001 C CNN
F 3 "" H 8750 5925 50  0001 C CNN
	1    8750 5925
	1    0    0    -1  
$EndComp
Wire Wire Line
	8650 5825 8650 5875
Wire Wire Line
	8650 5875 8750 5875
Wire Wire Line
	9050 5875 9050 5825
Wire Wire Line
	8750 5825 8750 5875
Connection ~ 8750 5875
Wire Wire Line
	8750 5875 8850 5875
Wire Wire Line
	8750 5875 8750 5925
Wire Wire Line
	8850 5875 8850 5825
Connection ~ 8850 5875
Wire Wire Line
	8850 5875 9050 5875
Text Label 7325 5125 0    50   ~ 0
LRCLK
Text Label 7325 5225 0    50   ~ 0
BCLK
Text Label 7325 5325 0    50   ~ 0
DIN
Wire Wire Line
	8650 4825 8650 4725
Wire Wire Line
	8750 4725 8750 4825
Text Label 7325 5525 0    50   ~ 0
VDD
Text Label 7325 5425 0    50   ~ 0
GND
Text Label 6750 3950 2    50   ~ 0
LRCLK
Text Label 6750 3850 2    50   ~ 0
BCLK
Text Label 6750 3750 2    50   ~ 0
DIN
Wire Wire Line
	8650 4725 8700 4725
Text Label 8700 4650 0    50   ~ 0
VBATT
Wire Wire Line
	8700 4650 8700 4725
Connection ~ 8700 4725
Wire Wire Line
	8700 4725 8750 4725
$Comp
L Device:C_Small C101
U 1 1 5C6B87A9
P 10350 3925
F 0 "C101" H 10442 3971 50  0000 L CNN
F 1 "0.1uF" H 10442 3880 50  0000 L CNN
F 2 "Capacitor_SMD:C_0201" H 10350 3925 50  0001 C CNN
F 3 "~" H 10350 3925 50  0001 C CNN
F 4 "CL03A104KQ3NNNH" H 10350 3925 50  0001 C CNN "MPN"
	1    10350 3925
	1    0    0    -1  
$EndComp
Wire Wire Line
	10350 3650 10350 3825
Wire Wire Line
	9625 3650 10350 3650
$Comp
L power:GND #PWR0108
U 1 1 5C6BB914
P 10350 4125
F 0 "#PWR0108" H 10350 3875 50  0001 C CNN
F 1 "GND" H 10355 3952 50  0000 C CNN
F 2 "" H 10350 4125 50  0001 C CNN
F 3 "" H 10350 4125 50  0001 C CNN
	1    10350 4125
	1    0    0    -1  
$EndComp
$Comp
L Device:C_Small C103
U 1 1 5C6BB989
P 9650 2600
F 0 "C103" H 9742 2646 50  0000 L CNN
F 1 "0.1uF" H 9742 2555 50  0000 L CNN
F 2 "Capacitor_SMD:C_0201" H 9650 2600 50  0001 C CNN
F 3 "~" H 9650 2600 50  0001 C CNN
F 4 "CL03A104KQ3NNNH" H 9650 2600 50  0001 C CNN "MPN"
	1    9650 2600
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 5C6BBB04
P 9650 2750
F 0 "#PWR0109" H 9650 2500 50  0001 C CNN
F 1 "GND" H 9655 2577 50  0000 C CNN
F 2 "" H 9650 2750 50  0001 C CNN
F 3 "" H 9650 2750 50  0001 C CNN
	1    9650 2750
	1    0    0    -1  
$EndComp
Wire Wire Line
	8925 2200 9650 2200
Wire Wire Line
	9650 2200 9650 2500
Connection ~ 8925 2200
Wire Wire Line
	8925 2200 8925 2500
Wire Wire Line
	9650 2750 9650 2700
Wire Wire Line
	10350 4125 10350 4075
$Comp
L Device:C_Small C102
U 1 1 5C6C7669
P 10725 3925
F 0 "C102" H 10817 3971 50  0000 L CNN
F 1 ".0022uF" H 10817 3880 50  0000 L CNN
F 2 "Capacitor_SMD:C_0201" H 10725 3925 50  0001 C CNN
F 3 "~" H 10725 3925 50  0001 C CNN
F 4 "GRM033R71E222KA12D" H 10725 3925 50  0001 C CNN "MPN"
	1    10725 3925
	1    0    0    -1  
$EndComp
Wire Wire Line
	10725 3550 10725 3825
Wire Wire Line
	9625 3550 10725 3550
Wire Wire Line
	10350 4075 10725 4075
Wire Wire Line
	10725 4075 10725 4025
Connection ~ 10350 4075
Wire Wire Line
	10350 4075 10350 4025
$Comp
L Device:R R103
U 1 1 5C9D189F
P 3925 1250
F 0 "R103" H 3995 1296 50  0000 L CNN
F 1 "10k" H 3995 1205 50  0000 L CNN
F 2 "Resistor_SMD:R_0402_1005Metric" V 3855 1250 50  0001 C CNN
F 3 "~" H 3925 1250 50  0001 C CNN
	1    3925 1250
	1    0    0    -1  
$EndComp
Wire Wire Line
	3925 1400 3925 1650
$Comp
L power:VDD #PWR0110
U 1 1 5C9D18AD
P 3925 975
F 0 "#PWR0110" H 3925 825 50  0001 C CNN
F 1 "VDD" H 3942 1148 50  0000 C CNN
F 2 "" H 3925 975 50  0001 C CNN
F 3 "" H 3925 975 50  0001 C CNN
	1    3925 975 
	1    0    0    -1  
$EndComp
Wire Wire Line
	3925 975  3925 1100
Text Label 3925 1650 0    50   ~ 0
EN
$EndSCHEMATC
