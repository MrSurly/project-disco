
import machine

i = machine.I2C(scl=machine.Pin(33), sda=machine.Pin(32))
buf = bytearray(1)
i.readfrom_mem_into(104, 0x75, buf)


import mpu6050

m = mpu6050.MPU6050()
while 1: m.read_sensors_scaled()


from machine import I2S
from machine import Pin

bck_pin = Pin(19)   # Bit clock output
ws_pin = Pin(21)    # Word clock output
sdout_pin = Pin(18) # Serial data output

audio_out = I2S(I2S.NUM1,                                  # create I2S peripheral to write audio
                bck=bck_pin, ws=ws_pin, sdout=sdout_pin,    # sample data to an Adafruit I2S Amplifier
                standard=I2S.PHILIPS, mode=I2S.MASTER_TX,  # breakout board,
                dataformat=I2S.B16,                        # based on MAX98357A device
                channelformat=I2S.ONLY_RIGHT,
                samplerate=16000,
                dmacount=16,dmalen=512)


samples = bytearray(4096)
j = 0

import math
i = 0
while i < len(samples):
    x = int(math.sin(j) * 8192 + 32768)
    samples[i+1] = x >> 8
    samples[i+0] = x & 0xff
    j += .05
    i += 2
    


num_bytes_written = audio_out.write(samples)              # write audio samples to amplifier
                                                          # note:  blocks until sample array is emptied
                                                          # - see optional timeout argument
                                                          # to configure maximum blocking duration



